# NodeJs notes

Install process is via package manager on linux `sudo dnf install nodejs` `sudo apt-get install nodejs` etc..

> Node.JS is a javascript runtime built on Google's V8 Javascript engine

JS is natively understood by browsers, making the browser the JS runtime. NODE.JS makes it possible to execute JS code outside the browser. The V8 engine is used to actually parse and execute the JS within NODE.JS. This extends the abilities of JS, making it reasonable to use JS on the server-side of web development.

- NODE.JS is single-threaded, based on event driven, non-blocking I/O model
  - It's fast and easily scalable
  - Good for data-intensive apps
- Simpler to use the same language across the entire stack
- Suitable for production workloads
- NPM Packages are magical
- Active Developer Community

Use Cases

- API with a db backend
- Data streaming (video/audio)
- Real-time chat applications
- Server-side webApps.
  - All content generated on the server

Not-Use Cases

- Applications with heavy server-side processing (CPU intensive workloads)
  - Rails, PHP, Python are better for these kinds of apps

## Node REPL

> REPL stands for: Read Eval Print Loop

The REPL is simply a CLI that allows and processes JS.

To enter the repl use the `node` command, with no arguments

```js
┌─[david@fedora] - [~/code/complete-node-bootcamp/1-node-farm/starter] - [Tue Aug 10, 16:26]
└─[$]> node
Welcome to Node.js v14.17.0.
Type ".help" for more information.
> const name='david'
undefined
> name
'david'
> 8*8
64
> .exit
// List of available modules in the REPL
> //<tab><tab>
Array                 ArrayBuffer           Atomics               BigInt                BigInt64Array         BigUint64Array        Boolean               Buffer
DataView              Date                  Error                 EvalError             FinalizationRegistry  Float32Array          Float64Array          Function
Infinity              Int16Array            Int32Array            Int8Array             Intl                  JSON                  Map                   Math
NaN                   Number                Object                Promise               Proxy                 RangeError            ReferenceError        Reflect
RegExp                Set                   SharedArrayBuffer     String                Symbol                SyntaxError           TextDecoder           TextEncoder
TypeError             URIError              URL                   URLSearchParams       Uint16Array           Uint32Array           Uint8Array            Uint8ClampedArray
WeakMap               WeakRef               WeakSet               WebAssembly           _                     _error                assert                async_hooks
buffer                child_process         clearImmediate        clearInterval         clearTimeout          cluster               console               constants
crypto                decodeURI             decodeURIComponent    dgram                 diagnostics_channel   dns                   domain                encodeURI
encodeURIComponent    escape                eval                  events                fs                    global                globalThis            http
http2                 https                 inspector             isFinite              isNaN                 module                net                   os
parseFloat            parseInt              path                  perf_hooks            process               punycode              querystring           queueMicrotask
readline              repl                  require               setImmediate          setInterval           setTimeout            stream                string_decoder
sys                   timers                tls                   trace_events          tty                   undefined             unescape              url
util                  v8                    vm                    wasi                  worker_threads        zlib

__defineGetter__      __defineSetter__      __lookupGetter__      __lookupSetter__      __proto__             hasOwnProperty        isPrototypeOf         propertyIsEnumerable
toLocaleString        toString              valueOf

constructor

// use the underscore to reference the previous result
> 3*8
24
> _+6
30
> _-28
2
>
// tab completion will work within Methods as well.
> String.//<tab><tab>
String.__defineGetter__      String.__defineSetter__      String.__lookupGetter__      String.__lookupSetter__      String.__proto__             String.hasOwnProperty
String.isPrototypeOf         String.propertyIsEnumerable  String.toLocaleString        String.valueOf

String.apply                 String.arguments             String.bind                  String.call                  String.caller                String.constructor
String.toString

String.fromCharCode          String.fromCodePoint         String.length                String.name                  String.prototype             String.raw

> String.
```

To execute a file use the command `node /path/to/file`

## Core Modules

Modules add functionality to the base NODE.JS application. [NODE.JS v14.7.4 Docs](https://nodejs.org/dist/latest-v14.x/docs/api/)

To be able to utilize a module the module must be specified using `require` e.g. `const fs = require('fs');` to require the `fs` module

## More Modules

User defined modules can be imported by the `require` function specifying the direct path to the module: `const replaceTemplate = require('./1-node-farm/modules/replaceTemplate');`

Modules are exported using the `modules.exports` function.

## NPM and Packages

[NPM Website](https://www.npmjs.com/)

NPM comes pre-installed with `node`. `npm init` will generate the `package.json` file which contains metadata about the application, including version, author, application name, etc.. The most important information in the `package.json` file is the list of packages required by the application to properly function.

Dependency Types:

- Simple
  - Packages that contain code used in the application
    - Code _depends_ on these packages to function
- Development
  - Used for development
    - testing libraries
    - code bundlers
  - Code doesn't depend on these to function

NPM Commands

- `npm install <packageName>` will install the package and update the `package.json` file
  - `npm i <packageName>` is a functional shorthand for the `install` command
- `npm i <package>@<version>` will install the specified version of a package.
- `npm install` will install all packages required by an application, and their dependencies
- `npm update <package>` will update to the latest package.
- `npm uninstall <package>` will remove a package

Packages will be imported into `node_modules` directory, all dependencies of any installed packages. This folder might get big.

NPM allows for a global install, by default packages are installed locally (just in the current project folder) Global install will likely require `sudo` permissions. `npm i <packageName> --global` will install a package globally

If a package is installed locally, calling the dependancy directly form the CLI won't work. Instead you have to define a start script in the package.json file. Using these start scripts will utilize the local dependency vs the global script

```json
  "scripts": {
    "start": "nodemon index.js"
  },
```

To call this script use `npm run start` from within the project folder.

### Package Versions

Most NPM packages use the `<major>.<minor>.<patch>` versioning scheme e.g. `1.18.11`. Patches are bug fixes only, Minors are new non-breaking feature, Major versions are big updates, with potentially breaking changes. In `package.json` versions are listed like: `"nodemon": "^2.0.12"` the carat (`^`) at the beginning of the version number indicates that only minor and patch updates will be applied when updating packages. Replacing the `^` with a `~` will only accept patch updates, while a `*` will accept **all** new updates including major versions

### The `node_modules` directory

This directory contains all node modules, but should never be included in a git repository or any other non-local storage. Using the `npm install` command will automatically pull all of the appropriate packages. You should always share the `package.json` and `package-lock.json` files, as NPM will use these files to determine which packages to pull down.

## Blocking & Non-Blocking, Sync & Async

Synchronous == Blocking

Synchronous Code:

```js
const fs = require('fs');

const textIn = fs.readFileSync('./1-node-farm/txt/input.txt', 'utf-8');
console.log(textIn);

const textOut = `This what we know about the avocado ${textIn}.\nCreated on ${Date.now()}`;
fs.writeFileSync('./1-node-farm/txt/output.txt', textOut);
console.log('File has been written');
```

Each line of code waits on the previous line to complete, before execution

Asynchronous code executes in the background, and is called later using a `callback` function. Each async function requires two pieces, the time consuming function, and the callback. NODE.JS is designed around using callbacks. Other applications get a single-thread per instance, not per application.

The NODE.JS process is a single thread in in the CPU. Each access to the application uses the same thread. So when a user specific process blocks the code, it interrupts all other users attempting to access the application.

Just because a `callback` exists, that does _not_ mean that the function is an async function.

Callback hell is a real thing, where 1 call back depends on another, which depends on a 3 third process...and so on. Using `Promises` or `Async/Await` functions can help prevent this scenario.

Callback example:

```js
//Non-blocking, async
fs.readFile('./1-node-farm/txt/start.txt', 'utf-8', (err, data) => {
  console.log(data);
});

console.log('Will read file!');
```

Output:

```sh
┌─[david@fedora] - [~/code/node-bootcamp] - [Wed Aug 11, 07:33]
└─[$]> node index.js
Will read file!
read-this
```

The callback function `(err, data) => {console.log(data);}` takes two arguments. The first `err` is where any errors are stored, the second `data` is where the results of the async function, in this example the contents of the text file. Treat the arguments like variables, `err` and `data` are conventions, but are not required, as long as they are referenced properly.

Callback hell example:

```js
//Non-blocking, async.  Callback hell
fs.readFile('./1-node-farm/txt/start.txt', 'utf-8', (err, data1) => {
  fs.readFile(`./1-node-farm/txt/${data1}.txt`, 'utf-8', (err, data2) => {
    console.log(data2);
    fs.readFile(`./1-node-farm/txt/append.txt`, 'utf-8', (err, data3) => {
      console.log(data3);

      fs.writeFile(
        `./1-node-farm/txt/final.txt`,
        `${data2}\n${data3}`,
        'utf-8',
        (err) => {
          console.log('Your file has been written!! =) ');
        }
      );
    });
  });
});

console.log('Will read file!');
```

## Creating a Simple webserver (http Module)

The `http` module is used for accepting and responding to http requests sent to the server (API?)

There is access to two functions required for this to work the `req` and `res` functions. The `req` function accepts requests and the `res` function responds to the requests. Use the `createServer` method to enable the server

Setting up a simple webserver:

```js
const server = http.createServer((req, res) => {
  res.end('Hello from the Server!');
});

server.listen(8000, '127.0.01', () => {
  console.log('Listening on port 8000');
});
```

Results:

```sh
┌─[david@fedora] - [~/code/node-bootcamp] - [Wed Aug 11, 07:49]
└─[$]> node index.js
Listening on port 8000

# This is done from a seperate terminal since the app was called manually, and not triggered to run in the background
┌─[david@fedora] - [~/code/node-bootcamp] - [Tue Aug 10, 16:38]
└─[$]> curl localhost:8000
Hello from the Server!%
```

## Routing

Routing implements different actions for different URLs, so http://my.server/info provides different content than http://my.server/go-away-dog.

> For complex websites a tool like Express should be used to simplify life

Simple routing is done with `if-else` statements. Taking the url requested and returning different results for each url. as seen below:

```js
const server = http.createServer((req, res) => {
  const pathName = req.url;

  if (pathName === '/' || pathName === '/overview') {
    res.end('This is the overview');
  } else if (pathName === '/product') {
    res.end('This is the product');
  } else {
    res.writeHead(404, {
      'Content-type': 'text/html',
      'my-own-header': 'hello-world',
    });
    res.end('<h1>Page Not found!</h1>');
  }
});
```

### Parsing URLs

`console.log(url.parse(req.url, true))` will return the the URL object the user requested

```sh
Listening on port 8000
# User requested the root web page, not much interesting
/
Url {
  protocol: null,
  slashes: null,
  auth: null,
  host: null,
  port: null,
  hostname: null,
  hash: null,
  search: null,
  query: [Object: null prototype] {},
  pathname: '/',
  path: '/',
  href: '/'
}

# Here the user requested a product page.
/product?id=0
Url {
  protocol: null,
  slashes: null,
  auth: null,
  host: null,
  port: null,
  hostname: null,
  hash: null,
  search: '?id=0',
  query: [Object: null prototype] { id: '0' },  #<-- the query.id property will show which individual property was requested
  pathname: '/product',  # <-- This shows us it was a 'product' requested
  path: '/product?id=0',
  href: '/product?id=0'
}
```

Using the `parse.url` method we can take information from the url passed by the user and act accordingly, like loading a product page for a specific product.

```js
// declared at the beginning of the https server definition
const { query, pathname } = url.parse(req.url, true);
// this pulls the 'query' field and the 'pathname' field from the parsed url.  These fields are defined by the parsing function, and aren't dev-defined fields.  So console.log the output to ensure you're pulling the right fields

// in the http route for /product
const product = dataObj[query.id];
// We're using the 'replaceTemplate' function we built to replace everything in the template-product.html file with the information from the query.id field, which we're calling 'product' as defined in the row above.
const output = replaceTemplate(tempProduct, product);
```

## HTML Templates

Instead of writing an HTML page for each unique thing, we can write an HTML template, and then dynamically replace information as things change, or to serve information dynamically based on what the user requests. These html template files go in the `./templates` directory. There are mutliple ways to replace the data in the template, in this example we're using regex, so each dynamic field is a string of characters that's not going to appear anywhere other than where we want to replace them. In this case we're using a consistent format because that's the best way to make sure you don't screw something up. The format is: `{%VARIABLE_NAME%}` See example below:

```html
<figure class="card">
  <div class="card__emoji">{%IMAGE%}{%IMAGE%}</div>
  <div class="card__title-box">
    <h2 class="card__title">{%PRODUCT_NAME%}</h2>
  </div>
</figure>
```

Notice the `{%IMAGE%}` and `{%PRODUCT_NAME%}` fields. The `replaceTemplate` function (below) is used to replace each occurance of those fields with a value from an object

Here we have part of the `replaceTemplate` function. It takes two inputs: `temp` and `product`. `temp` is the template that should be called to have values replaced (see HTML above), product is the specific item from the object that should be used to provide the values.

```js
const replaceTemplate = (temp, product) => {
    let output = temp.replace(/{%PRODUCT_NAME%}/g, product.productName);
    output = output.replace(/{%IMAGE%}/g, product.image);
```

This function is called in our script like this: `const output = replaceTemplate(tempProduct, product);` In this case we're providing `tempProduct` to the `temp` value in the function, and `product` goes to `product`. We get `tempProduct` from earlier in the script where we define it: `const tempProduct = fs.readFileSync(`${\_\_dirname}/1-node-farm/templates/template-product.html`, 'utf-8');` So `tempProduct` is just a reference to the `template-product.html` file. `product` on the other hand is a direct reference to the page the user is attempting to load `const product = dataObj[query.id];` (See Parsing URLs, in the Routes section). So we provide the template to be changed, and the product ID to be used when changing the template. We do have to be using a json object for this to work properly. All said, this means that all we need to do to update our web page with new products/prices/information, is update the .json file where all of our product information is kept. (or database or other location)
